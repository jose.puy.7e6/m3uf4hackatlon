﻿using System;
using System.Collections.Generic;

namespace M3UF4Hackatlon_GelmaElfo_PuyJose
{
    class Program
    {
        public interface IRecolectable
        {
            public void recoger();
        }

        public interface IAtacable
        {
            public void recibirDamage(int damage);

            public bool morir();

            public void atacar(IAtacable obj);
        }

        public class Tesoro : IRecolectable
        {
            public string Nombre { get; set; }

            public int Valor { get; set; }

            public Tesoro(string nombre, int valor)
            {
                Nombre = nombre;

                Valor = valor;
            }

            public void recoger()
            {
                Console.WriteLine(Nombre + " x" + Valor);
            }
        }

        abstract public class Personaje : IAtacable
        {
            public string Nombre { get; set; }

            public int Vida { get; set; }

            public int Ataque { get; set; }

            public Personaje(string nombre, int vida, int ataque)
            {
                Nombre = nombre;

                Vida = vida;

                Ataque = ataque;
            }

            public void recibirDamage(int damage)
            {
                Vida -= damage;
            }

            public bool morir()
            {
                if (Vida <= 0)
                {
                    return true;
                }
                return false;
            }

            public void atacar(IAtacable obj)
            {
                Personaje atacable = (Personaje)obj;
                atacable.recibirDamage(Ataque);
                atacable.morir();
            }
        }

        public class Jugador : Personaje
        {
            public List<IRecolectable> Inventario { get; set; }

            public Jugador(string nombre, int vida, int ataque) : base(nombre, vida, ataque)
            {
                Inventario = new List<IRecolectable>();
            }

            public void moverse(string direccion)
            {
                Console.WriteLine("El personaje se mueve hacia " + direccion);
            }

            public void recogerTesoro(IRecolectable obj)
            {
                Tesoro tesoro = (Tesoro)obj;
                Inventario.Add(tesoro);
                tesoro.recoger();
            }

            public void mostrarInventario()
            {
                Console.WriteLine("\nINVENTARIO:");
                foreach (Tesoro tesoro in Inventario)
                {
                    tesoro.recoger();
                }
            }
        }

        public class Enemigo : Personaje
        {
            public Tesoro Recompensa { get; set; }

            public Enemigo(Tesoro tesoro, string nombre, int vida, int ataque) : base(nombre, vida, ataque)
            {
                Recompensa = tesoro;
            }

            public void drop()
            {
                Recompensa.recoger();
            }
        }

        public class Juego
        {
            static void Main()
            {


                Jugador Saitama = new Jugador("Saitama", 500, 1000000);

                Jugador Zote = new Jugador("Zote el Todopoderoso", 5, 2);

                Tesoro PataDeTortuga = new Tesoro("Pata de Tortuga", 4);

                Tesoro CristalQuiral = new Tesoro("Cristal Quiral", 25);

                Enemigo Koopa = new Enemigo(PataDeTortuga, "Koopa", 10, 2);

                Enemigo EnteVarado = new Enemigo(CristalQuiral, "Ente Varado", 30, 75);

                List<Enemigo> enemigos = new List<Enemigo>();

                enemigos.Add(Koopa);
                enemigos.Add(EnteVarado);

                // Partida con un Jugador y dos Enemigos, la cual termina con el Jugador derrotado.
                //Partida(Zote, enemigos);


                // Partida con un Jugador y dos Enemigos, la cual termina con el Jugador ganando.
                Partida(Saitama, enemigos);

                Saitama.mostrarInventario();

            }

            // Metodo que sirve para empezar una partida con un Personaje y una Lista de Enemigos introducidos por parmetros.
            static void Partida(Jugador player, List<Enemigo> enemies)
            {
                int num = 0;

                do
                {
                    Console.WriteLine("Hacia donde quieres moverte?");
                    Enfrentamiento(player, enemies[num]);
                    num++;
                    if (num >= enemies.Count)
                    {
                        Console.WriteLine("No quedan enemigos en estas tierras!");
                        break;
                    }
                } while (!player.morir());
            }

            // Metodo que genera un enfrentamiento contra un enemigo, incluyendo la comparacion de Ataque y la toma de decision.
            static void Enfrentamiento(Jugador player, Enemigo enemy)
            {
                string direccion = Console.ReadLine();
                player.moverse(direccion);

                Console.WriteLine("¡¡HA APARECIDO UN ENEMIGO!!");

                Comparar(player, enemy);

                Console.WriteLine("Que quieres hacer? [0 - Huir] [1 - Luchar]");
                int input = Convert.ToInt32(Console.ReadLine());

                switch (input)
                {
                    case 0:
                        break;
                    default:
                    case 1:
                        Pelea(player, enemy);
                        break;
                }
            }

            static void Comparar(IAtacable jugador, IAtacable enemigo)
            {
                Personaje player = (Personaje)jugador;
                Personaje enemy = (Personaje)enemigo;

                if (player.Ataque > enemy.Ataque)
                {
                    Console.WriteLine($"El ataque de {player.Nombre} es superior al del enemigo {enemy.Nombre}.");
                }
                else if (player.Ataque < enemy.Ataque)
                {
                    Console.WriteLine($"El ataque del enemigo {enemy.Nombre} es superior al del jugador {player.Nombre}.");
                }
                else
                {
                    Console.WriteLine("Tienen el mismo ataque.");
                }
            }

            // Metodo iterativo que enfrenta a un jugador con un enemigo, hasta que uno de los 2 cae derrotado.
            static void Pelea(IAtacable jugador, IAtacable enemigo)
            {
                Jugador player = (Jugador)jugador;
                Enemigo enemy = (Enemigo)enemigo;

                while (!player.morir() && !enemy.morir())
                {
                    enemy.atacar(player);
                    if (!player.morir())
                    {
                        player.atacar(enemy);
                    }
                }
                if (player.morir())
                {
                    Console.WriteLine("Has muerto!");
                }
                if (enemy.morir())
                {
                    Console.WriteLine($"Has derrotado al enemigo {enemy.Nombre}");
                    player.recogerTesoro(enemy.Recompensa);
                }
            }
        }
    }
}
